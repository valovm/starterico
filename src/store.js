import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    languages: {
      current: "RU",
      variants: [
        {
          value: "ru",
          label: "RU"
        },
        {
          value: "en",
          label: "EN"
        }
      ]
    },
    profile: {
      variants: ["bearcoin", "mrbrcoin"],
      current: "bearcoin"
    }
  },
  mutations: {

  },
  actions: {

  }
})
