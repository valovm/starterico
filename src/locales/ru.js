export default {

  ru: {
      YES: 'Да',
      FROM: 'от',
      UP_TO: 'до',

      HOME:{
      TITLE: 'SDFGFDG',
      INTRO: 'Конесервативная стратегия долгосрочного инвестирования в криптовалюты и ICO c выпуском собственных токенов.',
      INTERFACE_CONTENT: 'Интерфейс Starterico позволяет с легостью инвестировать и  полностью контролировать совершенные инвестииции.',
          COUNTRIES_TITLE: 'ТОП 5 стран',
          COUNTRIES_CONTENT: 'В каких странах наиболее популярен 24starterico.com ?',
          ABOUT_TITLE: `Криптовалюты<BR>без конвертации к доллару`,
          ABOUT_CONTENT: `
                Среди бесчисленного множества различных токенов есть по-настоящему прибыльные проекты, дающие заработать «иксы» профита.   
          `,

      },



    ABOUT: {
      TITLE: 'O проекте',
      SUBTITLE: 'Starterico — первый онлайновый инвестиционной криптофонд.',
      INTRO: ``,
      CONTENT: `
        <p>Его цель – предоставить всем желающим возможность открыть себе депозит в bitcoin по аналогии с банковским вкладом и оперировать им как валютой реализовавшегося будущего. Криптодепозитарий Starterico дает своим инвесторам двойную выгоду – заработок на росте bitcoin и дивиденды от вклада.</p>
        <p>Это время мы сконцентрировали колоссальный опыт в данной сфере. На сегодняшний день мы обладаем внушительным торговым капиталом, превышающим порог в $150.000.000 и эта сумма постоянно увеличивается в своих объёмах. Но перед тем как достигнуть успеха, нам пришлось пройти очень сложный путь. На протяжении долгих лет основатели компании работали в различных брокерских фирмах и на собственном опыте убедились в том, что услуги доверительного управления часто сопровождаются достаточно высокой комиссией и приносят доход, не соответствующий ожиданиям инвесторов. Именно поэтому было принято решение собрать все лучшее, что было до этого на рынке, дополнить проект новыми уникальными инвестиционными продуктами и создать компанию совершенно нового формата.</p>
      `,
      DOCUMENTS_CONTENT: 'Компания Starterico зарегистрирована в офшорной зоне Белиза, это обеспечивает нам налоговую свободу и прочную основу для осуществления эффективной и легальной деятельности в западных регионах мира. Мы стремительно растем и развиваемся, осваивая новые рынки, так, в 2016 году, советом директоров было принято решение расширить зону своего влияния на европейском континенте и открыть штаб-квартиру в Великобритании.',
    },


    PARTNER_PROGRAM:{
      TITLE:'',
      INTRO:'',
      STATUS_1: {
        NAME: 'Базовый',
        NAMES: 'Базовые',
        BODY: '',
      },
      STATUS_2: {
        NAME: 'VIP',
        NAMES: 'VIP',
        BODY: '',
      },
      STATUS_3: {
        NAME: 'Супр Лидер',
        NAMES: 'Супер Лидеры',
        BODY: '',
      },
    },


    OPEN_DEPOSIT_PAGE:{
      TITLE: "Открыть депозит",
    },
    OPEN_DEPOSIT_FORM:{
      INPUT_AMOUNT_DEPOSIT: 'Введите сумму депозита',
      SELECT_PLAN: 'Выберите тарифный план',
      SELECT_PAYMENT_SYSTEM: 'Выберите платежную систему',
    },
    WITHDRAW_PAGE:{
      TITLE: 'Вывести средства',
    },
    WITHDRAW_FROM:{
      TO: 'Куда',
      AMOUNT: 'Сколько'
    },

    FOOTER:{
      CONTENT: 'CoinsBank Named cards issuer is Wave Crest Holdings Limited. The My Choice Visa Prepaid card is issued by Wave Crest Holdings Limited pursuant to a license from Visa Europe. Visa is a registered trademark of Visa Incorporated. Wave Crest Holdings Limited is a licensed electronic money institution by the Financial Services Commission, Gibraltar.\n' +
      'Site operated by CB Exchange LP, financial services provided by XBIT LTD, No.1 Orchid St, Belmopan, Belize. Under license of IFSC Belize and FinCEN MSB registration. PCI DSS validated, credit card safe.'
    },

    ALL: 'Все',
    SUPPORT_TITLE: 'Поддержка Online',
    NEW_TICKET: "Новый тикет",
    NEW_TICKET_BTN: "Создать тикет",
    ALL_TICKETS: "Все",
    WAITING_ANSWER: "Ждут ответа",
    ANSWER_RECEIVED: "Ответ получен",
    TICKET_CLOSED: "Тикет закрыт",

    BACK_BTN: 'Назад',
    SEARCH_BTN: 'Поиск',
    SEND_BTN: 'Отправить сообщение',
    DEMO_BTN: 'DEMO_BTN',
    MORE_BTN: 'MORE_BTN',
    REINVEST_BTN: 'Реинвестировать',
    REG_BTN: 'Зарегистрироваться',
    JOIN_BTN: 'JOIN_BTN',
    START_BTN: 'START_BTN',

    RESET_PASSWORD_LINK: 'Забыли пароль?',

    PARTNERS: 'Партнеры',
    WITHDRAW: 'Вывести',
    WALLETS: 'Кошельки',
    SUPPORT: 'Поддержка',
    LOGOUT: 'Выйти',

    BALANCE: 'Баланс',


    ADD_MONEY: "Пополнить",
    ALL_TRANSACTIONS: "Все транзакции",
    RECENT_TRANSACTIONS: "Транзакции",

    CREATE_DEPOSIT:'Создать депозит',
    CREATE_NEW_DEPOSIT: 'Создать новый депозит',
    CREATE_DEPOSIT_BTN: 'Создать',
    DEPOSIT: 'Депозит',
    CLOSE_DEPOSIT: 'Закрыть депозит',

    ALL_DEPOSITS: "Все депозиты",
    CLOSED_DEPOSITS: 'Закрытые депозиты',
    ACTIVE_DEPOSITS: 'Aктивные депозиты',

    NEXT_ACCRUAL: 'Ближайшее начисление через',
    NEXT_ACCRUALS: 'Будет начислено',
    ACCRUAL: 'Начисление',

    CREATED_AT: 'Создан',


    INVESTED: 'Вложено',
    EARNED: 'Заработано',

    TRANSACTIONS_TITLE: 'История транзакций',
    CONTACTS_TITLE: "Контакты",
    
    REINVEST: {
      TITLE: 'Реинвестировать',
    },


    TYPE_OPERATION: 'Вид операции',
    PAYMENT_SYSTEM: 'Платежная система',
    REFERAL: 'Реферал',

    PER_DAY: 'в день',
    PER_WEEK: 'в неделю',
    PER_MONTH: 'в месяц',

    AFTER_WEEK: 'через неделю',
    AFTER_MONTH: 'через месяц',

    DEPOSIT_CAN_WITHDRAW: 'Депозит можно снять через 24 часа',
    WITHDRAW_COMMISSION: 'Комиссия при снятии депозита',
    ACTIVE_DEPOSITS_IS_NOT_LIMITED: 'Количество активных депозитов не ограничено',
    PROFIT_WITHOUT_CONVERSION: 'Криптовалюта без конвертации (Прибыль именно в монетах)',


    WITHDRAW_BTN: 'Вывести средства',
    WITHDRAW_MONEY: 'Вывести средства',
    MIN_WITHDRAW: 'Минимальная сумма выплаты',
    WITHDRAW_NOTICE: `
      <p></p>
    `,


    WALLETS_TITLE: 'Ваши реквизиты',
    WALLETS_NOTICE: 'В целях безопасности заменить платежные реквизиты необходимо написать письмо на admin@we-pay-every-day.com с почты указанной при регистрации.',

    REFSYS_TITLE: 'Реферальная система',
    YOUR_PARTNER_LINK: "Ваша партнерская ссылка",
    PARTNER_LINK: "Партнерская ссылка",

    YOUR_STATUS: 'Ваш статус:',
    USERS_OPEN_DEPOSIT: 'Создали депозит',
    X_USERS_LEFT: 'приглашенных',
    X_USERS_LEFT_TO: 'приглашенных до',

    TOTAL_AMOUNT_OF_DEPOSITS: 'Инвестировали рефералы',
    EARNED_COMMISSION: 'Заработано комиссии',
    EARNED_MONEY: 'Заработано в проекте',

    YOU_WERE_INVITED_BY:  'Вас пригласил',

    PARTNER_NETWORK: 'Ваша структура',
    ALL_PARTNERS: 'Ваша структура',


    CHECK_COMPANY: 'Проверить компанию',

    PROFIT: 'Прибыль',
    TOTAL: 'Итого',

    LAST_PAYMENTS: 'Свежие выплаты',
    LAST_DEPOSITS : 'Новые депозиты',


    NET_PROFIT: 'Чистая прибыль',
    AFTER_XXX_DAYS: 'Через {days} дней',


    FROM_PROFIT: 'от всех начислений прибыли',
    COUNT_ACTIVE_PARTNERS: 'активных лично приглашенных человек',

    REGISTER_NOW: "Зарегистрироваться сейчас",

    AFTER_SIGNUP: 'при регистрации',

    PERCENT_AND_DEPOSIT_IN_END_TERM: 'Начисление процентов и возврат депозита в конце срока',
    SAVE_SETTINGS_BTN: 'Сохранить настройки',
    CHANGE_PASSWORD_BTN: 'Сменить пароль',



    STATS:{
        USERS: 'В проекте',
        USERS_ONLINE: 'Онлайн',
        USERS_LABEL: 'чел.',
        NEW_USER: 'Новый пользователь',
        LAST_DEPOSIT: 'Последний депозит',
        SIGNUPS_24HOURS: 'Регистраций за 24 часа',
        MONEY_OUT: 'Выплачено',
        MONEY_IN: 'Инвестировано',
        PERCENT_TIMELINE: 'Процент в день',
        REG_TIMELINE: 'Количество регистраций участников в сутки',
        USERS_TOP: 'Топ клиентов Starterico',
        NEW_DEPOSIT: 'Новый депозит',
    },
    PLAN:{
        DEPOSIT_AMOUNT: ' Сумма депозита',
        DAILY_ACCRUALS: 'Ежедневные начисления',
        PERCENT_WORKDAYS: 'По будням',
        PERCENT_WEEKEND: 'В выходные',
        PERCENT_OR_DAY: 'Процент дня',
        TOTAL_PROFIT: 'Прибыль',
        AUTO_ACCRUALS: 'Автоматические выплаты',
        PAYOUTS_INCLUDE_DEPOSIT: 'Депозит включен в выплаты',
        WEEKLY_ACCRUALS: 'Еженедельные выплаты',
        MONTHLY_ACCRUALS: 'Ежемесячные выплаты',
        TERM: 'Срок'
    },
    SECURITY_SETTINGS:{
      TITLE: 'Настройки безопасности',
      AUTO_EXIT: 'Автоматически выйти через 20 минут',
      MULTI_SESSIONS: 'Запретить паралельные сессии',

    },

  CHANGE_PASSWORD:{
      TITLE: 'Изменить пароль',
      OLD_PASSWORD: 'Старый пароль',
      NEW_PASSWORD: 'Новый пароль',
      PASSWORD_CONFIRM: 'Введите паль еще раз',
  },


    SLIDER: {
      SLIDE_1: {
        TEASER: 'Через 20 дней',
        SMALL_TEASER: 'чистая прибыль'
      },
      SLIDE_2: {
        TEASER: 'Уже с нами',
        SMALL_TEASER: 'с декабря 2018'
      }
    },

    LOGIN_INPUT: "Логин",
    LOGIN_BTN: "Войти",
    LOGIN_BTN_DEMO: "Войти в демо-кабинет",
    PASSWORD: 'Пароль',
    REMEMBER: 'Запомнить',
    PASSWORD2: 'Повторите пароль',
      INPUT_OPTIONAL: "Необязательно",


    I_AGREE_RULES: 'Я принимаю правила и соглашаюсь с условиями предоставления услуг',

    CONFIRM_BTN: 'Подтвердить',
    ATTENTION: 'Внимание',


    REG: {
      TITLE: 'Регистрация',
      COMPLETE_TITLE: 'Регистрация завершена!',
      COMPLETE_TEXT: 'Теперь Вы можете войти в свой аккаунт',
      NOT_COMPLETE_TITLE: 'Регистрация НЕ завершена!',
      NOT_COMPLETE_TEXT: 'Для завершения операции Вы должны подтвердить свой e-mail.',
      NOT_COMPLETE_SMS_TEXT: 'Для завершения операции Вы должны подтвердить свой номер телефона',
      STOPING_TITLE: 'Регистрация приостановлена!',
      STOPING_TEXT: `   
        <p>Регистрация на сайте временно приостановлена.</p>
        <p>По всем вопросам обращайтесь в поддержку</a></p>
        `,
      LOGIN_INPUT: 'Придумайте логин',
      PASSWORD: 'Придумайте пароль',
      SECURE_SYSTEM: "Система безопсности",
      INPUT_WALLETS: "Укажите кошельки",


      BY_INVITE_TEXT: `
        <p>Регистрация на сайте возможна <b>только</b> по приглашению</p>
        <p>Для этого Вы должны перейти на наш сайт по специальной реф-ссылке участника или указать его логин или код приглашения</p>
        `,
    },

    LOGIN: {
      TITLE: 'Войти в кабинет',

    },

    CONFIRMATION:{
      TITLE: "Подтверждение",
    },

    RESET_PASSWORD:{
      TITLE: 'Восстановление пароля',
      BTN: 'Восстановить',
    },

    PROMO: {
      TITLE: 'Промо материалы',
    },

    RULES: {
      TITLE: 'Правила системы',
      CONTENT: ``
    },

    OPER:{
      CASHIN:'Пополнение',
      CASHOUT:'Вывод',
      GIVE:'Вклад',
      TAKE:'Снятие',
      CALCIN:'Начисление',
      REF:'Рефские',
      BONUS: 'Бонус'
    },



    on: 'Вкл',
    off: 'Выкл',
    Subject: 'Тема',
    Time: 'Время',
    Number: 'Номер',
    Message: 'Сообщение',
    Amount: 'Сумма',
    Status: 'Статус',
    Price: 'Цена',

    //
    psys_empty: 'Выберите платежную систему',
    sum_wrong: 'Неверная сумма',
    plan_wrong: 'Неверный план',
    wallet_not_defined: 'Заполните платежные реквизиты, пожалуйста',

    login_password_empty: "укажите Логин/Пароль",
    login_not_found: "неверная пара Логин/Пароль",
    not_active: 'e-mail аккаунта не подтвержден',
    banned: "доступ в аккаунт приостановлен",
    blocked:  'аккаунт заблокирован',
    GA_wrong: 'неверный Google Authenticator код',


    login_empty:'укажите логин',
    login_shor: "логин слишком короткий",
    login_wrong:  'неверный формат логина',
    login_used: 'логин уже используется',


    mail_empty:'укажите e-mail',
    mail_wrong:'неверный e-mail',
    mail_used:'e-mail уже используется',

    pass_empty:'укажите пароль',
    pass_short:"пароль слишком короткий",
    pass_wrong:'пароль не соответствует формату',
    pass_not_equal:'пароли не совпадают',

    tel_wrong: 'неверный номер',

    inv_empty:  'укажите код',
    inv_not_found:  'неверный код',
    inv_used: 'код уже использован',

    ref_empty:"укажите Логин",
    ref_not_found: "Логин не найден",

    must_agree: 'вы должны принять правила',

    code_empty:'укажите код',
    code_not_found: 'неверный код',
    code_used:  'код уже испован',
    code_expired:'код истек',
    dif_ip: 'dif_ip',
    oper_wrong:'некорректная транзакция',
    oper_unkn: 'транзакция не была завершена',

    'Enter code': 'Введите код',

    ERROR_404_TITLE: "Ошибка 404" ,
    PAGE_NOT_FOUND: "Страница не найдена",
    PARE: "Пара",
    QUANTITY: "Количество",
    BRING_ON_BALANCE: "Количество",
    TEXT_ABOUT_TIME: 'Осуществляется мгновенно',

    FIRST_NAME: "Имя",
    LAST_NAME: "Фамилия",
    FEEDBACK: 'Обратная связь',
    EMAIL: 'Электронная почта',
    PHONE: 'Телефон',


    RISKS: 'Риски',
  },
}
