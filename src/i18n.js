import Vue from 'vue'
import VueI18n from 'vue-i18n'


const dateTimeFormats = {
  'en': {
    short: {
      year: 'numeric', month: 'short', day: 'numeric'
    },
    long: {
      year: 'numeric', month: 'short', day: 'numeric',
      weekday: 'short', hour: 'numeric', minute: 'numeric'
    },
    
  },
  'ru': {
    short: {
      year: 'numeric', month: 'short', day: 'numeric'
    },
    long: {
          year: 'numeric', month: 'short', day: 'numeric',
          weekday: 'short', hour: 'numeric', minute: 'numeric', hour12: true
    }
  }
}

const numberFormats = {
  'en': {
    currency: {
      style: 'currency', currency: 'RUB'
    },
  },
  // 'ru': {
  //   currency: {
  //     style: 'currency', currency: 'USD'
  //   },
  // }
}


Vue.use(VueI18n);

import langs_ru from './locales/ru'

export default new VueI18n({
  numberFormats,
  locale: 'ru', // set locale
  messages: langs_ru, // set locale messages
  dateTimeFormats,
});

/*
export default new VueI18n({
  numberFormats,
  locale: process.env.VUE_APP_I18N_LOCALE || 'en',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en',
    dateTimeFormats,
  messages: loadLocaleMessages()
});

function loadLocaleMessages () {
  const locales = require.context('./locales', true, /[A-Za-z0-9-_,\s]+\.js/i)
  const messages = {}
  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i)
    if (matched && matched.length > 1) {
      const locale = matched[1]
      messages[locale] = locales(key)
    }
  })
  return messages
}


*/
