import Vue from 'vue'
import Router from 'vue-router'
import mainHeader from './components/panels/mainHeader.vue'
import cabinetHeader from './components/panels/cabinetHeader.vue'

Vue.use(Router)

const router = new Router({
  //mode: 'history',

  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      components: {
        default: () => import('./views/Home.vue'),
        header: mainHeader
      }
    },
    {
      path: '*',
      name: '404',
      components: {
        default: () => import('./views/404.vue'),
        header: mainHeader
      }
    },
  {
      path: '/faq',
      name: 'faq',
      components: {
          default: () => import('./views/FAQ'),
          header: mainHeader
      }
  },
      {
          path: '/stats',
          name: 'stats',
          components: {
              default: () => import('./views/Stats'),
              header: mainHeader
          }
      },
      {
          path: '/payouts',
          name: 'payouts',
          components: {
              default: () => import('./views/Payouts'),
              header: mainHeader
          }
      },
      {
          path: '/contacts',
          name: 'contacts',
          components: {
              default: () => import('./views/Contacts'),
              header: mainHeader
          }
      },
    {
      path: '/about',
      name: 'about',
      components: {
        default: () => import('./views/About.vue'),
        header: mainHeader
      }
    },
    /*{
      path: '/news',
      name: 'news',
      components: {
        default: () => import('./views/News.vue'),
        header: mainHeader
      }
    },
     */
    {
      path: '/partners',
      name: 'partners',
      components: {
        default: () => import('./views/Partners.vue'),
        header: mainHeader
      }
    },
    {
      path: '/agreement',
      name: 'agreement',
      components: {
        default: () => import('./views/Agreement.vue'),
        header: mainHeader
      }
    },
    {
      path: '/auth',
      name: 'auth',
      components: {
        default: () => import('./views/LoginPage.vue'),
        header: mainHeader
      },
      children: [{
          path: 'login',
          name: 'login',
          component: () => import('./components/panels/login.vue'),
        },
        {
          path: 'signup',
          name: 'signup',
          component: () => import('./components/panels/registration.vue'),
        },
        {
          path: 'reset',
          name: 'reset',
          component: () => import('./components/panels/reset.vue'),
        }
      ]
    },

    {
      path: '/dashboard/transactions',
      name: 'transactions',
      components: {
        default: () => import('./views/Dashboard/Transactions.vue'),
        header: cabinetHeader
      }
    },
    /*
    {
      path: '/support',
      name: 'support',
      components: {
        default: () => import('./views/Support.vue'),
        header: cabinetHeader
      }
    },
     */
    {
      path: '/dashboard/partnership',
      name: 'partnership',
      components: {
        default: () => import('./views/Partnership.vue'),
        header: cabinetHeader
      }
    },
    {
      path: '/dialog',
      name: 'dialog',
      components: {
        default: () => import('./views/Dialog.vue'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard/wallets',
      name: 'wallets',
      components: {
        default: () => import('./views/Wallets.vue'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard/investments',
      name: 'investments',
      components: {
        default: () => import('./views/Investments.vue'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      components: {
        default: () => import('./views/Dashboard/index/index'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard/deposits',
      name: 'deposits',
      components: {
        default: () => import('./views/Deposits'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard/withdraw',
      name: 'output',
      components: {
        default: () => import('./views/Withdraw'),
        header: cabinetHeader
      }
    },
    {
      path: '/dashboard/profile',
      name: 'profile',
      components: {
        default: () => import('./views/Profile.vue'),
        header: cabinetHeader
      }
    },
  ],
  scrollBehavior() {
    return {
      x: 0,
      y: 0,
    }
  },

})



export default router
