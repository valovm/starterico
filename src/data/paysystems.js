export const currs = {
    btc: {
        shortName: 'BTC',
        cost: 120,
        img: 'btc.svg',
    },
    ltc: {
        shortName: 'LTC',
        cost: 12,
        img: 'ltc.svg',
    },
    eth: {
        shortName: 'ETH',
        cost: 1,
        img: 'eth.svg',
    },
    dash: {
        shortName: 'DASH',
        img: 'dash.svg',
        cost: 1,
    },
    doge: {
        shortName: 'DOGE',
        img: 'dogecoin.svg',
        cost: 1,
    },
    usd: {
        shortName: 'USD',
        img: 'usd.svg',
        cost: 1,
    },
    euro: {
        shortName: 'euro',
        img: 'euro.svg',
        cost: 1,
    },
    rub: {
        shortName: 'RUB',
        img: 'rub.svg',
        cost: 67,
    }
};

export default {
    btc: {
        name: 'Bitcoin',
        img: 'btc.svg',
        curr: currs.btc
    },
    ltc: {
        name: 'Litecoin',
        img: 'ltc.svg',
        curr: currs.ltc
    },
    eth: {
        name: 'Etherium',
        img: 'eth.svg',
        curr: currs.eth
    },
    dash: {
        name: 'Dashcoin',
        img: 'dash.svg',
        curr: currs.doge
    },
    doge: {
        name: 'Dogecoin',
        img: 'dogecoin.svg',
        curr: currs.dash
    },
    payeer: {
        name: 'Payeer',
        img: 'payeer.svg',
        curr: currs.usd
    },
    payeer_rub: {
        name: 'Payeer Rub',
        img: 'payeer.svg',
        curr: currs.rub
    },
    pm: {
        name: 'PerfectMoney',
        img: 'pm.svg',
        curr: currs.usd
    },
    nix: {
        name: 'NIX money',
        img: 'nix.svg',
        curr: currs.usd
    }

}
