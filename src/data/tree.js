export default [{
  id: 1,
  role: 'INVESTOR',
  label: 'Alejandro Green',
  invested: 500,
  earned: 1500,
  userpic: 'ava1.png',
  children: [{
    id: 4,
    role: 'MARKETER',
    label: 'Billy Ball',
    invested: 5100,
    earned: 11500,
    userpic: 'ava1.png',
    children: [{
      id: 9,
      role: 'INVESTOR',
      label: 'Derek Richardson',
      invested: 5020,
      earned: 15020,
      userpic: 'ava1.png',
    }, {
      id: 10,
      role: 'PROGRAMMER',
      label: 'Lee Riley',
      invested: 100,
      earned: 200,
      userpic: 'ava1.png',
      children: [{
        id: 11,
        role: 'INVESTOR',
        label: 'John Galt',
        invested: 100,
        earned: 200,
        userpic: 'ava1.png',
      }, {
        id: 12,
        role: 'MARKETER',
        label: 'Brian Black',
        invested: 100,
        earned: 200,
        userpic: 'ava1.png',
        children: [{
          id: 13,
          role: 'INVESTOR',
          label: 'Jack Gold',
          invested: 10,
          earned: 20,
          userpic: 'ava1.png',
        }, {
          id: 14,
          role: 'PROGRAMMER',
          label: 'Robert White',
          invested: 0,
          earned: 0,
          userpic: 'ava1.png',
        }]
      }]
    }]
  }]
}, {
  id: 2,
  role: 'PROGRAMMER',
  label: 'Bradley Miller',
  invested: 2000,
  earned: 5000,
  userpic: 'ava1.png',
  children: [{
    id: 5,
    role: 'INVESTOR',
    label: 'Garrett Munoz',
    invested: 1000,
    earned: 1300,
    userpic: 'ava1.png',
  }, {
    id: 6,
    role: 'INVESTOR',
    label: 'Edgar Harrington',
    invested: 120,
    earned: 220,
    userpic: 'ava1.png',
  }]
}, {
  id: 3,
  role: 'MARKETER',
  label: 'Harry Burke',
  invested: 2000,
  earned: 3000,
  userpic: 'ava1.png',
  children: [{
    id: 7,
    role: 'MARKETER',
    label: 'Stanley Warren',
    invested: 10,
    earned: 50,
    userpic: 'ava1.png',
  }, {
    id: 8,
    role: 'INVESTOR',
    label: 'Gary Jenkins',
    invested: 100,
    earned: 500,
    userpic: 'ava1.png',
  }]
}];