export default [
  {
    count: 5000,
    prefix: 'bit'
  },
  {
    count: 5500,
    prefix: 'lit'
  },
  {
    count: 7000,
    prefix: 'eth'
  },
  {
    count: 5000,
    prefix: 'pay'
  },
  {
    count: 2000,
    prefix: 'nix'
  },
  {
    count: 1000,
    prefix: 'per'
  },
]
