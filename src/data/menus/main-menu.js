export default [
    // {text: 'Home', url: {name: 'home'}},
    {text: 'About', url: {name: 'about'}, icon_svg: ''},
    {text: 'FAQ', url: {name: 'faq'}},
    // {text: 'News', url: {name: 'news'}},
    {text: 'Stats', url: {name: 'stats'}},
    {text: 'Payouts', url: {name: 'payouts'}},
    {text: 'Partners', url: {name: 'partners'}},
    {text: 'Contacts', url: {name: 'contacts'}},
];
