export default [
    {text: 'Balance', url: {name: 'buy-tokens'}, icon_svg: 'cross'},
    {text: 'Output money', url: {name: 'output'}, icon_svg: 'bag'},
    {text: 'To invest', url: {name: 'investments'}, icon_svg: 'stix'},
];
