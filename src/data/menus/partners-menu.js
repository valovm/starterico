export default [
    {text: 'Dashboard', url: {name: 'dashboard'}, icon_svg: 'settings'},
    {text: 'DEPOSITS', url: {name: 'deposits'}, icon_svg: 'stix'},
    {text: 'ADD_MONEY', url: {name: 'investments'}, icon_svg: 'cross' },

    {text: 'Transactions', url: {name: 'transactions'}, icon_svg: 'list'},
    {text: 'Output', url: {name: 'output'}, icon_svg: 'bag'},
    {text: 'Partners', url: {name: 'partnership'}, icon_svg: 'percent'},
];
