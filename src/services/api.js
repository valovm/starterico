import Vue from 'vue'

import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios)


const API_HOST = 'https://starterico.ru/sscr/api?';

const request = {
    post(path, data){
        return Vue.axios.post(`${API_HOST}${path}`, data)
    },
    get(path, ){
        return Vue.axios.get(`${API_HOST}${path}`,
            {
                headers: {
                    Authorization: "Bearer a7f30f70cfb31281a3a848c0db08afc4",
                    Accept: 'application/json, text/plain, */*',
                },
                withCredentials: false
            })
    }
};

const currentUser = function () {
    return request.get('/users/current');
};

const signIn = function ( email, password ) {
    return request.post('/auth/login', { email, password });
};


export const Api = {
    signIn,
    currentUser,
};
