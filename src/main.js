import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import './plugins/element.js'
import i18n from './i18n'

Vue.config.productionTip = false

import YandexShare from '@cookieseater/vue-yandex-share';
Vue.component('yandex-share', YandexShare);

import Vsvg from './components/Vsvg'
Vue.component('v-svg', Vsvg);

import smile from './components/smile'
Vue.component('smile', smile);

import vnDigit from './components/vn-digit'
Vue.component('vn-digit', vnDigit);

import vnRange from './components/vn-range'
Vue.component('vn-range', vnRange);

import vnLinkContact from './components/vn-link-contact'
Vue.component('vn-link-contact',vnLinkContact);

import vnPage from './components/PageTemplate'
Vue.component('vn-page', vnPage);

import vnMenu from './components/vn-menu'
Vue.component('vn-menu', vnMenu);

import MaskedInput from 'vue-masked-input';
Vue.component(MaskedInput);

import vnForminput from './components/vn-forminput';
Vue.component('vn-forminput', vnForminput);


import t from './components/t';
Vue.component('t', t);

import vnCopylink from './components/vn-copylink';
Vue.component('vn-copylink', vnCopylink);


Vue.filter('formatMoney', function (value) {
  if (typeof value !== "number") {
      return value;
  }
  var formatter = new Intl.NumberFormat('ru-RU', {
      minimumFractionDigits: 0
  });
  return formatter.format(value);
});


new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
